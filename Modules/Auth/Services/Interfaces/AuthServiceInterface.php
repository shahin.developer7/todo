<?php


namespace Modules\Auth\Services\Interfaces;

interface AuthServiceInterface
{
    public function check($data);
    public function getToken();
    public function getUserAuth();
    public function logout();
}
