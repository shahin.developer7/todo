<?php


namespace Modules\Auth\Services;


use Illuminate\Support\Facades\Auth;
use Modules\Auth\Services\Interfaces\AuthServiceInterface;

class AuthService implements AuthServiceInterface
{

    public function check($data)
    {
        return Auth::attempt(['email' => $data['email'], 'password' => $data['password']]);
    }

    public function getToken()
    {
        return $this->getUserAuth()->createToken('MyApp')->plainTextToken;
    }

    public function getUserAuth()
    {
        return Auth::user();
    }

    public function logout()
    {
        $this->getUserAuth()->tokens()->delete();
    }
}
