<?php

namespace Modules\Auth\Tests\Feature\Controller;

use Modules\User\Entities\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AuthControllerTest extends TestCase
{
    use RefreshDatabase;
    public function test_login_method()
    {
        $user = User::factory()->create();
        $this->withHeaders([
            'Accept' => 'application/json'
        ])->postJson('/api/login',[
            'email' =>  $user->email,
            'password' =>  'password',
        ])->assertOk();
    }
    public function test_login_validation()
    {
        $this->withHeaders([
            'Accept' => 'application/json'
        ])->postJson('/api/login')->assertStatus(422);
    }

    public function test_register_method()
    {
        $user = User::factory()->make();
        $this->withHeaders([
            'Accept' => 'application/json'
        ])->postJson('/api/register',$user->toArray())
            ->assertCreated()
            ->assertJsonStructure([
                "data"  =>  [
                            'id',
                            'name',
                            'email',
                        ]
                    ]);
        $this->assertDatabaseHas('users', [
            'email'=>$user->email
        ]);
    }
}
