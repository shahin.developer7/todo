<?php

namespace Modules\Auth\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Modules\Auth\Http\Requests\UserLoginRequest;
use Modules\Auth\Http\Requests\UserRegisterRequest;
use Modules\Auth\Services\Interfaces\AuthServiceInterface;
use Modules\User\Repositories\Interfaces\UserRepositoryInterface;
use Modules\User\Transformers\User;


class AuthController extends Controller
{

    protected $userRepository;
    protected $authService;

    public function __construct(UserRepositoryInterface $userRepository ,AuthServiceInterface $authService)
    {
        $this->userRepository = $userRepository;
        $this->authService = $authService;
    }

    public function login(UserLoginRequest $request)
    {
        $data = $request->only(['email','password']);
        if($this->authService->check($data)){
            return response()->json([
                "message"   => "welcome to project",
                'token'     =>  $this->authService->getToken(),
            ]);
        }
        else
            return response()->json(["message"=>"Unauthorised"],401);

    }

    public function register(UserRegisterRequest $request)
    {
        $data = $request->only(['name','email']);
        $data['password']=bcrypt($request->password);
        $user = $this->userRepository->store($data);
        return new User($user);
    }
    public function logout()
    {
        $this->authService->logout();
        return response()->json(['message' => 'Logged out']);
    }

}
