<?php


namespace Modules\Task\Repositories;



use App\Repositories\CrudRepository;
use Modules\Task\Entities\Task;
use Modules\Task\Repositories\Interfaces\TaskRepositoryInterface;

class TaskRepository extends CrudRepository implements TaskRepositoryInterface
{

    public function check()
    {
        return true;
    }

    protected function model()
    {
        return new Task();
    }
}
