<?php


namespace Modules\Task\Repositories\Interfaces;


use App\Repositories\Interfaces\CrudRepositoryInterface;

interface TaskRepositoryInterface extends CrudRepositoryInterface
{

}
