<?php

namespace Modules\Task\Http\Controllers\Api;

use Illuminate\Routing\Controller;
use Modules\Task\Http\Requests\TaskStoreRequest;
use Modules\Task\Http\Requests\TaskUpdateRequest;
use Modules\Task\Repositories\Interfaces\TaskRepositoryInterface;
use Modules\Task\Transformers\Task;
use Modules\Task\Transformers\TaskCollection;

class TaskController extends Controller
{
    private $taskRepository;
    public function __construct(TaskRepositoryInterface $taskRepository)
    {
        $this->taskRepository = $taskRepository;
    }

    public function index()
    {
        return new TaskCollection($this->taskRepository->get());
    }

    public function store(TaskStoreRequest $request)
    {
        $data = $request->only(['title','description']);
        $data['user_id'] = auth()->id();
        return new Task($this->taskRepository->store($data));
    }

    public function show($id)
    {
        return new Task($this->taskRepository->find($id));
    }

    public function update(TaskUpdateRequest $request, $id)
    {
        $data = $request->only(['title','description']);
        return new Task($this->taskRepository->update($id,$data));
    }

    public function destroy($id)
    {
        $this->taskRepository->delete($id);
        return response()->json(["message"=>'deleted']);
    }
}
