<?php

namespace Modules\Task\Tests\Feature\Model;

use Illuminate\Database\Eloquent\Model;
use Modules\Task\Entities\Task;
use Modules\User\Entities\User;
use Tests\Helpers\ModelHelper;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TaskTest extends TestCase
{
    use RefreshDatabase,ModelHelper;

    public function test_task_relation_user()
    {
        $task = Task::factory()->for(User::factory())->create();
        $this->assertTrue(isset($task->user->id));
        $this->assertTrue($task->user instanceof User);
    }

    protected function model(): Model
    {
        return new Task();
    }
}
