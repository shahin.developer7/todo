<?php

namespace Modules\Task\Tests\Feature\Controller;

use Modules\Task\Entities\Task;
use Modules\User\Entities\User;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class TaskControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_index_method()
    {
        $user = User::factory()->create();
        $this->actingAs($user,'sanctum')->withHeaders([
            'Accept' => 'application/json'
        ])->getJson('/api/tasks')->assertOk();

    }
    public function test_index_method_unauthorized()
    {
        $this->withHeaders([
            'Accept' => 'application/json'
        ])->getJson('/api/tasks')->assertUnauthorized();
    }

    public function test_store_method()
    {
        $user = User::factory()->create();
        $task = Task::factory()->for($user)->make();
        $this->actingAs($user,'sanctum')->withHeaders([
            'Accept' => 'application/json'
        ])->postJson('/api/tasks',$task->toArray())
            ->assertCreated()
            ->assertJsonStructure(
                [
                    'data' => [
                        'id',
                        'title',
                        'description',
                        'created_at',
                        'updated_at',
                    ]
                ]);
        $this->assertDatabaseHas('tasks', $task->toArray());

    }
    public function test_update_method()
    {
        $user = User::factory()->create();

        $task = Task::factory()->for($user)->create();

        $this->actingAs($user,'sanctum')->withHeaders([
            'Accept' => 'application/json'
        ])->putJson('/api/tasks/'.$task->id,[
            'title' =>  "tit",
            'description' =>  "desc",
            'user_id'     => $user->id
        ])
            ->assertOk()
            ->assertJson([
                "data" => [
                    "user_id"         => $task->user_id,
                    "title"           => "tit",
                    "description"     => "desc",
                ]
            ]);
    }

    public function test_delete_method()
    {
        $user = User::factory()->create();
        $task = Task::factory()->for($user)->create();
        $this->actingAs($user,'sanctum')->withHeaders([
            'Accept' => 'application/json'
        ])->deleteJson('/api/tasks/'.$task->id)
            ->assertStatus(200);
        $this->assertDatabaseMissing('tasks',$task->toArray());
    }
}
