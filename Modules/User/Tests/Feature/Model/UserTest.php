<?php

namespace Modules\User\Tests\Feauter\Model;

use Illuminate\Database\Eloquent\Model;
use Modules\Task\Entities\Task;
use Modules\User\Entities\User;
use Tests\Helpers\ModelHelper;
use Tests\TestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;

class UserTest extends TestCase
{
    use RefreshDatabase,ModelHelper;
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_user_relation_tasks()
    {
        $count = rand(1,10);
        $user = User::factory()->hasTasks($count)->create();
        $this->assertCount($count,$user->tasks);
        $this->assertTrue($user->tasks->first() instanceof Task);
    }

    protected function model(): Model
    {
        return  new User();
    }
}
