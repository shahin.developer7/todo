<?php


namespace Modules\User\Repositories\Interfaces;


use App\Repositories\Interfaces\CrudRepositoryInterface;

interface UserRepositoryInterface extends CrudRepositoryInterface
{
}
