<?php


namespace Modules\User\Repositories;


use App\Repositories\CrudRepository;
use Modules\User\Entities\User;
use Modules\User\Repositories\Interfaces\UserRepositoryInterface;


class UserRepository extends CrudRepository implements UserRepositoryInterface
{

    protected function model()
    {
        return new User();
    }
}
