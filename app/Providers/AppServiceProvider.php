<?php

namespace App\Providers;

use App\Repositories\CrudRepository;
use App\Repositories\Interfaces\CrudRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CrudRepositoryInterface::class,CrudRepository::class);

        $this->app->register(\Modules\Auth\Providers\ModuleServiceProvider::class);
        $this->app->register(\Modules\Task\Providers\ModuleServiceProvider::class);
        $this->app->register(\Modules\User\Providers\ModuleServiceProvider::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
